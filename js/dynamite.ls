/*
Dynamite: chained boolean group selection
Author: Peter Kruczkiewicz

Given a data table-like dataset, Dynamite allows users to select a subset of the
dataset using group selections from categories for multiple categories joined by
boolean operations such as AND, OR and NOT.

Required JS libraries:

- Underscore.js 1.6.0
- jQuery 1.9+
- Selectize.js
*/

class Node

  div_el: void
  div_el_id: ''
  base_data: {}
  node_data: {}
  is_root: false
  parent: void
  child: void
  category_select: void
  $category_select: void
  group_select: void
  $group_select: void
  categories: []
  groups: []
  sel_idx: []


  (@dynamite, @base_el, @parent=null) ->
    @base_data := @dynamite.data
    if @parent?
      # increment id index by 1 for child based off parent id index
      @id_idx := @parent.id_idx + 1
      @node_data := @parent.get_data_subset!
    else
      # root node has no parent
      @is_root := true
      @id_idx := 0
      # clone node data
      @node_data := {}
      for col of @base_data
        @node_data[col] = [x for x in @base_data[col]]

    @div_el_id := "dynamite_node_div_#{@id_idx}"
    
    @div_el := jQuery '<div/>' class: \dynamite_node, id: @div_el_id
      .appendTo @dynamite.el
    unless @is_root
      @$boolean_operator_select := jQuery '<select/>' class: 'boolean_operator'
        .appendTo @div_el

    @$category_select := jQuery '<select/>' class: \category_select, placeholder: 'Select a category...'
      .appendTo @div_el
    @$group_select = jQuery '<select/>' class: \group_select, multiple: \multiple, placeholder: 'Select one or more groups...'
      .appendTo @div_el

    category_selectize_args = 
      valueField: \value
      labelField: \text
      searchField: \text
      sortField: \count
      create: false
      onChange: (value) !~>
        if value.length == 0
          return
        @group_select.disable!
        @group_select.clearOptions!
        @group_select.load (callback) !~>
          xs = @get_groups_label_info value
          @group_select.enable!
          callback xs
        @dynamite.update_selection_description!

    if @is_root
      category_selectize_args.options = @get_column_label_info!

    @$category_select := @$category_select.selectize category_selectize_args
    @category_select := @$category_select[0].selectize
    
    # if not root node then add a boolean operator select input
    unless @is_root
      boolean_operators = <[ AND OR NOT ]>

      @$boolean_operator_select := @$boolean_operator_select.selectize do
        valueField: \value
        labelField: \text
        searchField: \text
        create: false
        options: [{text: x, value: x} for x in boolean_operators]
        onChange: (value) !~>
          unless value?
            return

          switch value
            when \AND
              @node_data := @parent.get_data_subset!
            when \NOT
              @node_data := @parent.get_data_subset!
            when \OR
              parent_data_subset = @parent.get_data_subset!
              parent_keys = parent_data_subset[@dynamite.key]
              idxs = [i for x, i in @base_data[@dynamite.key] when x in parent_keys]
              @node_data := {}
              for col of @base_data
                @node_data[col] = [x for x, i in @base_data[col] when i not in idxs]
          
          @dynamite.update_selection_description!
          
          @category_select.clearOptions!
          @category_select.load (callback) !~>
            xs = @get_column_label_info!
            callback xs
            
      @boolean_operator_select = @$boolean_operator_select[0].selectize
      # set AND operation as default operation across nodes
      @boolean_operator_select.setValue \AND
    
    @$group_select := @$group_select.selectize do
      valueField: 'value'
      labelField: 'text'
      searchField: 'text'
      sortField: 'count'
      plugins: ['remove_button']
      create: false
      onChange: (value) !~>
        unless value?
          @dynamite.disable_add_btn!
          return
        sel_category = @$category_select.val!
        sel_groups = @$group_select.val!

        @sel_idx := [i for x, i in @node_data[sel_category] when "#x" in sel_groups]
        unless @child?
          @dynamite.enable_add_btn!
        @dynamite.update_selection_description!

    @group_select := @$group_select[0].selectize
    @group_select.disable!

  get_boolean_operator: ->
    return @boolean_operator_select.getValue!

  get_column_label_info: ->
    cols = [{value: col, count: _.uniq @node_data[col] .length, text: "#{col} (n=#{_.uniq @node_data[col] .length})"} for col of @node_data]
    cols

  get_groups_label_info: (col) ->
    col_data = @node_data[col]
    counts = _.countBy col_data
    xs = [{value: "#x", text: "#x (n=#{counts[x]})", count: counts[x]} for x of counts]
    xs

  get_data_subset: ->
    if @sel_idx.length == 0
      return @node_data
    sel_category = @get_category_selection!
    bool_operation = if @is_root then \AND else @get_boolean_operator!
    switch bool_operation
      when \AND
        data_subset = {}
        for col of @node_data
          if col == sel_category and col != @dynamite.key
            continue
          data_subset[col] = [@node_data[col][idx] for idx in @sel_idx]
        return data_subset
      when \OR
        parent_data_subset = @parent.get_data_subset!
        parent_keys = parent_data_subset[@dynamite.key]
        node_keys = []
        for col of @node_data
          node_keys = [x for x, i in @node_data[@dynamite.key] when i in @sel_idx]
        union_keys = _.union parent_keys, node_keys
        idxs = [i for x, i in @base_data[@dynamite.key] when x in union_keys]
        data_subset = {}
        for col of @base_data
          data_subset[col] = [@base_data[col][idx] for idx in idxs]
        return data_subset
      when \NOT
        data_subset = {}
        for col of @node_data
          if col == sel_category and col != @dynamite.key
            continue
          data_subset[col] = [@node_data[col][idx] for key, idx in @node_data[@dynamite.key] when idx not in @sel_idx]
        return data_subset

  get_category_selection: ->
    @category_select.getValue!

  get_group_selection: ->
    @group_select.getValue!

  enable: !->
    @category_select.enable!
    @group_select.enable!
    unless @is_root
      @boolean_operator_select.enable!


  disable: !->
    @category_select.disable!
    @group_select.disable!
    unless @is_root
      @boolean_operator_select.disable!



export class Dynamite


  root_node: void

  (@el, @data, @key) ->

    @strains_metadata := {}
    for x, i in @data[@key]
      @strains_metadata[x] = {}
      for col of @data
        @strains_metadata[x][col] = @data[col][i]

    @root_node := new Node @, @el

    @btn_group := jQuery '<div/>' class: \btn_group
      .appendTo @el

    @btn_add_node := jQuery '<button/>' class: \add_node text: \Add
      .appendTo @btn_group
    @btn_add_node.prop \disabled true
    @btn_add_node.on \click !~> @add_new_node!

    @btn_remove_node := jQuery '<button/>' class: \remove_node text: \Remove
      .appendTo @btn_group
    @btn_remove_node.prop \disabled true
    @btn_remove_node.on \click !~> @remove_last_node!

    @selection_desc := jQuery '<div/>' class: \selection_desc text: "Selection description"
      .appendTo @btn_group

  disable_add_btn: !->
    @btn_add_node.prop \disabled true
  enable_add_btn: !->
    @btn_add_node.prop \disabled false
  disable_remove_btn: !->
    @btn_remove_node.prop \disabled true
  enable_remove_btn: !->
    @btn_remove_node.prop \disabled false

  get_leaf_node: (node) ->
    if node.child?
      return @get_leaf_node node.child
    return node

  update_selection_description: !->
    new_desc = @recurse_get_selection_desc "", @root_node
    @selection_desc.html new_desc

  recurse_get_selection_desc: (desc, node) ->
    if node.is_root
      desc += "[#{node.get_group_selection!.join ' OR '}] OF #{node.get_category_selection!}"
    else
      desc += "<br>#{node.get_boolean_operator!} [#{node.get_group_selection!.join ' OR '}] OF #{node.get_category_selection!} "
    if node.child?
      return @recurse_get_selection_desc desc, node.child
    return desc

  add_new_node: !->
    leaf_node = @get_leaf_node @root_node
    leaf_node.child = new Node @, @el, leaf_node
    @el.append @btn_group
    @disable_add_btn!
    @enable_remove_btn!
    leaf_node.disable!
    @update_selection_description!


  remove_last_node: !->
    leaf_node = @get_leaf_node @root_node
    if leaf_node == @root_node
      return

    leaf_node.div_el.remove!
    parent_node = leaf_node.parent
    @enable_add_btn!
    parent_node.child = null
    leaf_node = @get_leaf_node @root_node
    leaf_node.enable!
    if leaf_node == @root_node
      @disable_remove_btn!
    @update_selection_description!
  
  get_final_selection: ->
    leaf_node = @get_leaf_node @root_node
    console.log 'get_final_selection', leaf_node
    data_subset = leaf_node.get_data_subset!
    console.log 'leaf_node data_subset', data_subset
    data_subset[@key]